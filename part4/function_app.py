import azure.functions as func
import math

app = func.FunctionApp(http_auth_level=func.AuthLevel.ANONYMOUS)
def function_to_integrate(x):
    return abs(math.sin(x))

def numerical_integration(a, b):
    result = {}
    N = [10, 100, 1000, 10000, 100000]

    for i in N:
        h = (float(a) - float(b)) / i
        integral = 0
        for j in range(i):
            x2 = float(b) + h * (j + 0.5)
            I = abs(math.sin(x2)) * h
            integral += I
        result[i] = integral

    return str(result)

@app.route(route="numericalintegralservice")
def numerical_integration_service(req: func.HttpRequest) -> func.HttpResponse:
    try:
        lower = float(req.params.get('lower', 0.0))
        upper = float(req.params.get('upper', 3.14159))
    except ValueError as e:
        return func.HttpResponse("Invalid input. Please provide valid numeric values for 'lower' and 'upper'.", status_code=400)



    result = numerical_integration(upper,lower)
    

    return func.HttpResponse(f"Numerical integration results: {result}", status_code=200)