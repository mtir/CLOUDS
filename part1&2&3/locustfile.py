from locust import HttpUser, task, between

class QuickstartUser(HttpUser):
    @task
    def numericalintegralservice(self):
        response = self.client.get("/numericalintegralservice/0/3.14159")
        if response.status_code == 200:
            self.log_success("Request successful")
        else:
            self.log_failure("Request failed with status code: {}".format(response.status_code))
