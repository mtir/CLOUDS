from flask import Flask
import math

app = Flask(__name__)

@app.route('/numericalintegralservice/<b>/<a>')
def integration(b, a):
    result = {}
    N = [10, 100, 1000, 10000, 100000]

    for i in N:
        h = (float(a) - float(b)) / i
        integral = 0
        for j in range(i):
            x2 = float(b) + h * (j + 0.5)
            I = abs(math.sin(x2)) * h
            integral += I
        result[i] = integral

    return str(result)

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

