import azure.functions as func
import azure.durable_functions as df
from azure.storage.blob import BlobServiceClient

my_durable_app = df.DurableOrchestrationClient(df.DurableOrchestrationContext)

@my_durable_app.route(route="orchestrators/{function_name}")
@my_durable_app.durable_client_input(client_name="client")
async def start_orchestration(req: func.HttpRequest, client):
    function_name = req.route_params.get('function_name')
    instance_id = await client.start_new(function_name, None)
    response = client.create_check_status_response(req, instance_id)
    return response

@my_durable_app.orchestration_trigger(context_name="context")
def master_orchestrator(context):
    # Retrieve input data from an activity function
    input_data = yield context.call_activity("GetInputDataFn", "")
    # Print the retrieved input data
    print(input_data)
    # Fan-out: Map each input value using the mapper function
    map_lines = [context.call_activity("mapper", {key: value}) for key, value in input_data.items()]
    map_outputs = yield context.task_all(map_lines)
    # Fan-in: Shuffle the mapped outputs using the shuffler function
    shuffled_data = yield context.call_activity("shuffler", map_outputs)
    # Fan-out: Reduce the shuffled data using the reducer function
    reduce_words = [context.call_activity("reducer", {key: values}) for key, values in shuffled_data.items()]
    # Fan-in: Aggregate the reduced results
    final_result = yield context.task_all(reduce_words)
    return final_result

@my_durable_app.activity_trigger(input_name="input_data")
def mapper(input_data):
    output = []
    for i, j in input_data.items():

        word_bank = j.split()
        
        for word in word_bank:
        
            output.append((word, 1))
    
    return output

@my_durable_app.activity_trigger(input_name="map_outputs")
def shuffler(map_outputs):
    shuffle_dict = {}
    for output_list in map_outputs:
        for key, value in output_list:
            if key in shuffle_dict:
                shuffle_dict[key].append(value)
            else:
                shuffle_dict[key] = [value]
    return shuffle_dict

@my_durable_app.activity_trigger(input_name="shuffled_data")

def reducer(shuffled_data):
    # Initialize an empty dictionary to store the reduced result
    reduced_result = {}
    # Iterate through the items in the shuffled data
    for key, values in shuffled_data.items():
        total_value = sum(values)
        reduced_result[key] = total_value


    return reduced_result

@my_durable_app.activity_trigger(input_name="unused_data")
def get_input_data_fn(unused_data):
    connection_token = "DefaultEndpointsProtocol=https;AccountName=mymapreducefct;AccountKey=L0oazF3c/lcjPVXR25v67Z8axA2Hzz8GlfAc3Rxupe6pXW2HQEwluERKyMIxoCcHk9l0IaiEp1SG+ASt5lqLCw==;EndpointSuffix=core.windows.net"
    blob_service_client = BlobServiceClient.from_connection_string(connection_token)
    container_client = blob_service_client.get_container_client("mapreducedata")
    blobs = container_client.list_blobs()
    input_data_dict = {}
    offset = 0

    # Iterate through each blob to get the content (assuming text file) and build key-value pairs
    for blob in blobs:
        blob_client = container_client.get_blob_client(blob.name)
        blob_content = blob_client.download_blob().readall().decode('utf-8')
        for line in blob_content.splitlines():
            input_data_dict[str(offset)] = line
            offset += 1

    return input_data_dict
